#include "miniwin.h"
#include <stdbool.h>
#include <windows.h>
#include <stdio.h>
#include <time.h>

using namespace miniwin;

const int TAMA=20;
const int FILAS=20;
const int COLUMNAS=10;
typedef int tablero[COLUMNAS][FILAS];
struct Coord
{
    int x, y;
};
struct piezs
{
    Coord orig;
    Coord per[3];
    int color;

  Coord posicion(int n)const
    {
    Coord piezs;
    Coord ret={orig.x,orig.y};
    if(n!=0)
    {
        ret.x+=per[n-1].x;
        ret.y+=per[n-1].y;
    }
    return ret;
     }
};
void cuadrado (int x, int y
               )
{
 rectangulo_lleno (1+x*TAMA,
                   1+y*TAMA,
                   x*TAMA+TAMA,
                   y*TAMA+TAMA);
}
void col_pieza (const piezs&p)
{
color(p.color);
for (int m=0;m<4;m++)
{
    Coord c=p.posicion(m);
    cuadrado(c.x,c.y);
}

}
Coord rot_derecha(const Coord&c)
{
    Coord ret={-c.y,c.x};
    return ret;
}

void ro_derecha(piezs&p)
{
    for (int s=0;s<3;s++)
    {
        p.per[s]= rot_derecha(p.per[s]);
    }
}

void vac_tablero(tablero&t)
{
    for(int z=0;z<COLUMNAS;z++)
    {
        for (int i=0;i<FILAS;i++)
        {
            t[z][i]=NEGRO;
        }
    }
}
void col_tablero(const tablero&t)
{
    for(int z=0;z<COLUMNAS;z++)
    {
        for (int i=0;i<FILAS;i++)
        {
            color(t[z][i]);
            cuadrado(z,i);
        }
    }
}
void pon_pieza(tablero&t,const piezs&p)
{
    for (int u=0;u<4;u++)
    {
        Coord c=p.posicion(u);
        t[c.x][c.y]=p.color;
    }

}
bool colision_tablero(const tablero &t,const piezs &p)
{
  for(int u=0;u<4;u++)
  {
      Coord c=p.posicion(u);
      if (c.x<0||c.x >= COLUMNAS)
      { return true;
         }
         if (c.y<0||c.y >= FILAS)
         {
             return true;
         }
         if (t[c.x][c.y]!=NEGRO)
         {
             return true;
         }

}
return false;
}
const Coord perfs [7][3]=
{
  {{1,0},{0,1},{1,1}},//cuadrado
  {{1,0},{-1,1},{0,1}},//z
  {{0,1},{1,1},{-1,0}},//z2
  {{0,1},{0,-1},{1,1}},//l
  {{0,1},{0,-1},{-1,1}},//lr
  {{-1,0},{1,0},{0,1}},//t
  {{0,1},{0,-1},{0,2}},//|
};
void pieza_nueva(piezs&p)

{
    p.orig.x=5;
    p.orig.y=3;
    p.color=1+rand()%6;
    int r=rand()%7;
    for(int d=0;d<3;d++)
    {
        p.per[d]=perfs[r][d];
    }

}
bool fila_llena(const tablero&t, int fila)
{
for (int j=0;j<COLUMNAS;j++)
{
   if (t[j][fila]==NEGRO)
    return false;
}
return true;
}
void colap_tablero (tablero &t, int fil)
{
    for (int h=fil;h>0;h--)
    {
        for (int a=0;a<COLUMNAS;a++)
        {
            t[a][h]=t[a][h-1];
        }
    }
    for (int i=0; i<COLUMNAS;i++)
    {
        t[i][0]=NEGRO;
    }
}
int cuenta_lin(tablero&t)
{
    int fila=FILAS-1, cont=0;
    while(fila>=0)
    {
        if (fila_llena(t,fila))
        {
            colap_tablero(t,fila);
            cont++;
        }else
        {
          fila--;
        }

    }
    return cont;
}
int main()
{
    vredimensiona(TAMA*COLUMNAS,TAMA*FILAS);
    srand(time(0));
    int tic=0;

    tablero t;
    vac_tablero(t);
    col_tablero(t);

    piezs c;
    pieza_nueva(c);
    col_pieza(c);
    refresca();
    int k=tecla();
    while (k!=ESCAPE)
    {
        piezs copia=c;
        if (k==NINGUNA && tic>5)
        {
            tic=0;
            k=ABAJO;
        }
        if(k==ABAJO)
        {
            c.orig.y++;
        }
        else if(k==ARRIBA)
        {
            ro_derecha(c);
        }
        else if (k==IZQUIERDA)
        {
            c.orig.x--;
        }
        else if(k==DERECHA)
        {
            c.orig.x++;
        }
        if(colision_tablero(t,c))
        {
            c=copia;
             if (k==ABAJO)
        {
            pon_pieza(t,c);
            int cont=cuenta_lin (t);
            pieza_nueva(c);
        }
        if (colision_tablero(t,c))
        {
            espera(1000);
            vcierra();
        }
        }
        if (k!=NINGUNA)
        {
        borra();
        col_tablero(t);
        col_pieza(c);
        refresca();
        }
       espera(30);
       tic++;
        k=tecla();
    }
    vcierra();
    return 0;
}
